package com.abbtech.exchange.service;

import com.abbtech.exchange.dto.ExchangeRateDTO;
import com.abbtech.exchange.feignclient.ForexRateApiClient;
import com.abbtech.exchange.model.ForexRatesResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class CurrencyServiceImpl implements CurrencyService {

    private final ForexRateApiClient forexRateApiClient;

    @Value("${api.key}")
    private String apiKey;

    @Override
    public List<ExchangeRateDTO> calculateExchangeRates(String sourceCurrency, Double amount) {
        String baseCurrency = sourceCurrency.toUpperCase();
        ForexRatesResponse response = forexRateApiClient.getLatestRates(apiKey, baseCurrency);
        Map<String, Double> rates = response.getRates();

        List<ExchangeRateDTO> exchangeRates = new ArrayList<>();

        rates.forEach((currency, rate) -> {
            Double exchangeValue = amount * rate;
            exchangeRates.add(new ExchangeRateDTO(currency, rate, exchangeValue));
        });

        return exchangeRates;
    }

    @Override
    public ExchangeRateDTO calculateExchangeRate(String sourceCurrency, Double amount, String targetCurrency) {
        if (targetCurrency == null || targetCurrency.isEmpty()) {
            targetCurrency = "AZN";
        }
        List<ExchangeRateDTO> exchangeRates = calculateExchangeRates(sourceCurrency, amount);
        String finalTargetCurrency = targetCurrency;
        return exchangeRates.stream()
                .filter(rate -> rate.currencyCode().equalsIgnoreCase(finalTargetCurrency))
                .findFirst()
                .orElse(null);
    }
}
