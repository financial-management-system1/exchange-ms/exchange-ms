package com.abbtech.exchange.service;

import com.abbtech.exchange.dto.ExchangeRateDTO;

import java.util.List;

public interface CurrencyService {
    List<ExchangeRateDTO> calculateExchangeRates(String sourceCurrency, Double amount);
    ExchangeRateDTO calculateExchangeRate(String sourceCurrency, Double amount, String targetCurrency);
}
