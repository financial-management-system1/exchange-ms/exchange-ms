package com.abbtech.exchange.controller;

import com.abbtech.exchange.dto.ExchangeRateDTO;
import com.abbtech.exchange.dto.ExchangeRateRequest;
import com.abbtech.exchange.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping()
@RequiredArgsConstructor
public class ExchangeController {

    private final CurrencyService currencyService;

    @PostMapping("/converts")
    public List<ExchangeRateDTO> getExchangeRates(@RequestBody ExchangeRateRequest request) {
        return currencyService.calculateExchangeRates(request.sourceCurrency(), request.amount());
    }

    @PostMapping("/converted")
    public ExchangeRateDTO getExchangeRateWithTargetCurrency(@RequestBody ExchangeRateRequest request) {
        return currencyService.calculateExchangeRate(request.sourceCurrency(), request.amount(), request.targetCurrency());
    }
}
