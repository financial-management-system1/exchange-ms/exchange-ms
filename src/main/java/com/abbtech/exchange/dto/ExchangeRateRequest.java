package com.abbtech.exchange.dto;

public record ExchangeRateRequest(String sourceCurrency, Double amount, String targetCurrency) {
}
