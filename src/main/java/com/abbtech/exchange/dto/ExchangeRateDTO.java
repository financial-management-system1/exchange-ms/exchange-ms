package com.abbtech.exchange.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

public record ExchangeRateDTO(String currencyCode, @JsonIgnore Double exchangeValue, Double convertedAmount) {
}
