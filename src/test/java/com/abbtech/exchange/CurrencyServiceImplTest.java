package com.abbtech.exchange;

import com.abbtech.exchange.dto.ExchangeRateDTO;
import com.abbtech.exchange.feignclient.ForexRateApiClient;
import com.abbtech.exchange.model.ForexRatesResponse;
import com.abbtech.exchange.service.CurrencyServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

 class CurrencyServiceImplTest {

    @Mock
    private ForexRateApiClient forexRateApiClient;

    @InjectMocks
    private CurrencyServiceImpl currencyService;

    private final String testApiKey = "2f69b672b9163c648f6fe24b21020551";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCalculateExchangeRates() {
        ForexRatesResponse response = new ForexRatesResponse();
        response.setRates(Map.of("USD", 1.0, "EUR", 0.85));
        when(forexRateApiClient.getLatestRates(testApiKey, "USD")).thenReturn(response);

        List<ExchangeRateDTO> result = currencyService.calculateExchangeRates("USD", 100.0);
        assertEquals(2, result.size());

        ExchangeRateDTO usdExchangeRate = result.get(0);
        assertEquals("USD", usdExchangeRate.currencyCode());
        assertEquals(1.0, usdExchangeRate.exchangeValue());
        assertEquals(100.0, usdExchangeRate.convertedAmount());

        ExchangeRateDTO eurExchangeRate = result.get(1);
        assertEquals("EUR", eurExchangeRate.currencyCode());
        assertEquals(0.85, eurExchangeRate.exchangeValue());
        assertEquals(85.0, eurExchangeRate.convertedAmount());
    }

    @Test
    public void testCalculateExchangeRateWithTargetCurrency() {
        ForexRatesResponse response = new ForexRatesResponse();
        response.setRates(Map.of("USD", 1.0, "EUR", 0.85, "AZN", 1.7));
        when(forexRateApiClient.getLatestRates(testApiKey, "USD")).thenReturn(response);

        ExchangeRateDTO result = currencyService.calculateExchangeRate("USD", 100.0, "EUR");
        assertEquals("EUR", result.currencyCode());
        assertEquals(0.85, result.exchangeValue());
        assertEquals(85.0, result.convertedAmount());

        ExchangeRateDTO defaultResult = currencyService.calculateExchangeRate("USD", 100.0, "");
        assertEquals("AZN", defaultResult.currencyCode());
        assertEquals(1.7, defaultResult.exchangeValue());
        assertEquals(170.0, defaultResult.convertedAmount());
    }
}
