package com.abbtech.exchange;//package com.abbtech.exchange.controller;

import com.abbtech.exchange.controller.ExchangeController;
import com.abbtech.exchange.dto.ExchangeRateDTO;
import com.abbtech.exchange.dto.ExchangeRateRequest;
import com.abbtech.exchange.service.CurrencyService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ExchangeController.class)
 class ExchangeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CurrencyService currencyService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetExchangeRates() throws Exception {
        ExchangeRateRequest request = new ExchangeRateRequest("USD", 100.0, null);
        List<ExchangeRateDTO> mockResponse = Collections.singletonList(new ExchangeRateDTO("EUR", null, 85.0));

        when(currencyService.calculateExchangeRates("USD", 100.0)).thenReturn(mockResponse);

        mockMvc.perform(post("/exchangeRates")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"sourceCurrency\":\"USD\",\"amount\":100.0}"))
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"currencyCode\":\"EUR\",\"convertedAmount\":85.0}]"));
    }


    @Test
    void testGetExchangeRateWithTargetCurrency() throws Exception {
        ExchangeRateRequest request = new ExchangeRateRequest("USD", 100.0, "EUR");
        ExchangeRateDTO mockResponse = new ExchangeRateDTO("EUR", null, 85.0);

        when(currencyService.calculateExchangeRate("USD", 100.0, "EUR")).thenReturn(mockResponse);

        mockMvc.perform(post("/exchangeRate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"sourceCurrency\":\"USD\",\"amount\":100.0,\"targetCurrency\":\"EUR\"}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"currencyCode\":\"EUR\",\"convertedAmount\":85.0}"));
    }

}
