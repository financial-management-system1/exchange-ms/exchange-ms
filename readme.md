---

### Exchange-MS

---

# Exchange Rate Service

This service provides currency exchange rates and conversions.

## Technologies Used

* Spring Boot
* Feign Client
* External Forex API

## Configuration

Update application.properties with your API key and Forex API configurations.

## Endpoints

* /converts - Get exchange rates for a currency.
* /converted - Convert an amount from one currency to another.

## Running the Application

bash
./gradlew bootRun


## Testing

bash
./gradlew test
